package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"os/user"
	"path"
	"strings"
	"time"
)

type pippackage struct {
	Pipver  string
	Name    string
	Version string
}

func (pkg *pippackage) pkgRepoPath() string {
	return path.Join(getUserDataPath(), "repo", "pip"+pkg.Pipver, pkg.Name, pkg.Version)
}

type pkgmeta struct {
	Type            string
	Inited          bool // if we've ran the setup script yet
	SetupScriptPath *string
	ImportPath      *string
	Deps            *requirementsspec
}

func getUserDataPath() string {
	u, err := user.Current()
	if err != nil {
		panic(err)
	}
	if u.Uid == "0" || u.Username == "root" {
		return "/var/lib/vader"
	}
	return path.Join(u.HomeDir, ".vader")
}

func (pkg *pippackage) pkgRepoMetaPath() string {
	return path.Join(getUserDataPath(), "repo", "pip"+pkg.Pipver, pkg.Name, pkg.Version+".meta")
}

func (pkg *pippackage) getMeta() *pkgmeta {
	raw, _ := ioutil.ReadFile(pkg.pkgRepoMetaPath())
	var data pkgmeta
	err := json.Unmarshal(raw, &data)
	if err != nil {
		return nil
	}
	return &data // shrug?  why are we doing this?
}

func (pkg *pippackage) setMeta(pm pkgmeta) {
	raw, _ := json.MarshalIndent(pm, "", "\t")
	ioutil.WriteFile(pkg.pkgRepoMetaPath(), raw, 0644)
}

// Gives the path of the package after it's been built.  The "brand" argument
// is usually "lib", although it could be "bin".
func (pkg *pippackage) getPkgImportPath(brand string) *string {
	meta := pkg.getMeta()
	if meta == nil {
		return nil
	}
	return meta.ImportPath
}

func (pkg *pippackage) getDescriptorStr() string {
	return pkg.Pipver + ":" + pkg.Name + ":" + pkg.Version
}

// fix000Permissions searches through a directory tree and if it finds and files
// with 000 permissions sets them to 755.  Sometimes .tar.gz distributions have
// those as settings for some reason.
func fix000Permissions(p string) (uint32, error) {

	var cnt uint32

	dir, err := ioutil.ReadDir(p)
	if err != nil {
		return 0, err
	}

	for _, info := range dir {

		fpath := path.Join(p, info.Name())

		if info.IsDir() {

			if info.Mode()&0755 == 0 {
				fmt.Printf("Fixing 000 perms on %s\n", fpath)
				err := os.Chmod(fpath, 0755)
				if err != nil {
					return cnt, err
				}
				cnt++
			}

			subcnt, err := fix000Permissions(fpath)
			cnt += subcnt
			if err != nil {
				return cnt, err
			}

		} else {
			if info.Mode()&0644 == 0 {
				fmt.Printf("Fixing 000 perms on %s\n", fpath)
				err := os.Chmod(fpath, 0644)
				if err != nil {
					return cnt, err
				}
				cnt++
			}
		}

	}

	return cnt, nil
}

func downloadPackage(pkg pippackage) string {

	var err error

	tempdir, err := ioutil.TempDir("", "vader.")
	if err != nil {
		panic(err)
	}

	// Figure out what we should pass to pip as the version string.
	var dlstr = pkg.Name
	if len(pkg.Version) > 0 {
		dlstr = dlstr + "==" + pkg.Version
	}

	// Actually download it.
	dlcmd := exec.Command("pip"+pkg.Pipver, "download", "--no-deps", dlstr)
	dlcmd.Dir = tempdir
	dlcmd.Stdout = os.Stdout
	dlcmd.Stderr = os.Stderr
	dlcmd.Run()

	// Find the things we downloaded.
	files, err := ioutil.ReadDir(tempdir)
	if err != nil {
		panic(err)
	}

	// And extract the files we need.
	ppath := pkg.pkgRepoPath()
	os.MkdirAll(ppath, 0755)
	for _, f := range files { // This should only run once.

		var extcmd *exec.Cmd

		// Just blindly extract the file depending on the type.  We determine packaing format later.
		if strings.HasSuffix(f.Name(), ".tar.gz") {
			extcmd = exec.Command("tar", "-xzf", f.Name(), "--strip-components=1", "-C", ppath)
		} else if strings.HasSuffix(f.Name(), ".whl") || strings.HasPrefix(f.Name(), ".egg") {
			extcmd = exec.Command("unzip", "-q", f.Name(), "-d", ppath)
		} else if strings.HasSuffix(f.Name(), ".bz2") {
			extcmd = exec.Command("tar", "-xjf", f.Name(), "--strip-components=1", "-C", ppath)
		} else {
			fmt.Printf("Invalid package format for archive %s\n", f.Name())
			panic("unsupported package type")
		}

		// Now actually do it.
		extcmd.Dir = tempdir
		extcmd.Stdout = os.Stdout
		extcmd.Stderr = os.Stderr
		extcmd.Run()

	}

	// Make sure the permissions are set properly.  Sometimes this breaks.
	fixes, err := fix000Permissions(ppath)
	if err != nil {
		fmt.Printf("Error fixing permissions: %s\n", err.Error())
		panic("fixing permissions failure")
	}
	if fixes > 0 {
		fmt.Printf("Had to fix some 000 permissions: %d files fixed\n", fixes)
	}

	// Try to detect the package format.
	var format packagefmt
	for _, fmt := range getPackageFormats() {
		if fmt.isPathFormat(ppath) {
			format = fmt
		}
	}

	if format != nil {
		fmt.Printf("Detected format %s for package %s\n", format.getName(), pkg.getDescriptorStr())
	} else {
		fmt.Printf("Failed to determine format for package %s at path %s\n", pkg.getDescriptorStr(), ppath)
		panic("package format detection failed")
	}

	// Now parse it, fail if necessary.
	meta := format.parseMeta(ppath)
	if meta != nil {
		pkg.setMeta(*meta)
	} else {
		fmt.Printf("Metadata parse for package %s failed\n", pkg.getDescriptorStr())
		panic("metadata parse failed")
	}

	// Cleanup
	os.RemoveAll(tempdir)

	return ppath

}

func buildPackage(pkg pippackage, deps []pippackage) error {

	// First some sanity checks.
	meta := pkg.getMeta()
	if meta.Type == "wheel" {
		// wheels don't have build scripts so we can just ignore a call to this ever.
		return fmt.Errorf("wheels don't have build scripts")
	}

	if meta.Inited {
		fmt.Printf("Tried to init a build %s when already built.\n", pkg.getDescriptorStr())
		return fmt.Errorf("package already inited")
	}

	// Now figure out where the build script is.
	scriptPath := meta.SetupScriptPath
	if scriptPath == nil {
		panic("tried to build package without a build script!")
	}

	// This is just to build the package.
	dirpath := path.Dir(*scriptPath)
	invokePythonWithEnv(pkg.Pipver, []string{path.Base(*scriptPath), "build"}, deps, &dirpath)
	meta.Inited = true

	// Now try to find the import directory.
	builddir := path.Join(path.Dir(*scriptPath), "build")
	buildfiles, err := ioutil.ReadDir(builddir)
	if err != nil {
		panic(err)
	}

	// Actually loop through the fules looking for something to import.
	for _, f := range buildfiles {
		n := f.Name()
		fmt.Printf("%s\n", n)
		if strings.HasPrefix(n, "lib") || n == "lib" {
			libdir := path.Join(builddir, n)
			fmt.Printf("found import directory %s\n", libdir)
			meta.ImportPath = &libdir
			break
		}
	}

	pkg.setMeta(*meta)

	return nil

}

const pypiPackageURLFormat = "https://pypi.org/pypi/%s/json"

func getPypiPackageInfo(name string) map[string]interface{} {
	//url := fmt.Sprint(pypiPackageURLFormat, name)
	url := "https://pypi.org/pypi/" + name + "/json"

	fmt.Printf("getting package info from: %s\n", url)

	client := http.Client{
		Timeout: time.Second * 5,
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		panic(err)
	}

	req.Header.Set("User-Agent", "vader")

	res, getErr := client.Do(req)
	if getErr != nil {
		panic(getErr)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		panic(readErr)
	}

	var obj interface{}
	err = json.Unmarshal(body, &obj)
	if err != nil {
		fmt.Printf("error parsing response: %s\n", err.Error())
		panic("parse error")
	}

	root := obj.(map[string]interface{})
	return root
}

var pkglatestcached = map[string]string{}

func cachingGetPkgLatest(name string) string {
	if _, ok := pkglatestcached[name]; !ok {
		pkglatestcached[name] = getPkgLatestVersion(name)
	}
	return pkglatestcached[name]
}

func getPkgLatestVersion(name string) string {
	meta := getPypiPackageInfo(name)["info"].(map[string]interface{})
	return meta["version"].(string)
}

var pkgversionscached = map[string][]string{}

func cachingGetPkgVersions(name string) []string {
	if _, ok := pkgversionscached[name]; !ok {
		pkgversionscached[name] = getPkgVersions(name)
	}
	return pkgversionscached[name]
}

func getPkgVersions(name string) []string {
	versions := getPypiPackageInfo(name)["releases"].(map[string]interface{})
	keys := make([]string, 0)
	for k, a := range versions {
		arr := a.([]interface{})

		// we have to make sure there's actually downloads for the version
		if len(arr) > 0 {
			keys = append(keys, k)
		}
	}
	return keys
}
