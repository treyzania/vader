package main

import (
	"fmt"
	"strings"
	"testing"
)

var (
	allowed = []string{
		"1.0",
		"1!1.0",
		"1.0.post2",
		"1.2a3",
		"1.3b4.dev5",
		"1!1.2.3rc7.post42.dev1337",
	}
)

func TestIsPep440(t *testing.T) {

	for _, v := range allowed {
		if !isPep440(v) {
			println("failed on: " + v)
			t.Fail()
		}
	}

}

var (
	cvmChecks = map[string][]string{
		"1.0.*": []string{
			"1.0",
			"1.0.7",
			"1.0.dev3",
		},
	}
)

func TestCheckVerMatch1(t *testing.T) {

	for f, vs := range cvmChecks {
		for _, v := range vs {

			vv, _ := parseVersion(v)
			fv, _ := parseVersion(f)

			if !checkMatchingVersions(*vv, *fv) {
				println("failed match: filter = " + f + ", ver = " + v)
				t.Fail()
			}
		}
	}

}

var (
	verCompChecks = map[string]int{
		"1.0 1.0":             0,
		"1.0 1.0.1":           1,
		"1!1.0 2!1.0":         1,
		"1.0 1.0a2":           -1,
		"1.2.4.1.3 1.2.5.1.3": 1,
		"1.2.4.1.3 1.2.5.7.9": 1,
	}
)

func TestCompareVersions(t *testing.T) {

	for v, r := range verCompChecks {
		p := strings.Split(v, " ")

		//fmt.Printf("[A: %s, B: %s]\n", p[0], p[1])

		v0, _ := parseVersion(p[0])
		v1, _ := parseVersion(p[1])

		// Forward.
		res := compareVersions(*v0, *v1)
		if res != r {
			fmt.Printf("failed forward comparison: compareVersions(%s, %s) -> %d, expected %d\n", p[0], p[1], res, r)
			t.Fail()
		}

		// Try the reverse to make sure that works too.
		ires := compareVersions(*v1, *v0)
		if ires != -r {
			fmt.Printf("failed inverted comparsion: compareVersions(%s, %s) -> %d, expected %d\n", p[1], p[0], ires, -r)
			t.Fail()
		}

	}

}

// see pep440.go line ~36
var (
	ehh = "a"
	bee = "b"
	foo = "foo"
)

var parseVersionChecks = map[string]pep440version{
	"1.0": pep440version{
		epoch:        0,
		release:      []uint32{1, 0},
		wildcarded:   false,
		preType:      nil,
		preNumber:    -1,
		postNumber:   -1,
		devNumber:    -1,
		localVersion: nil,
	},
	"8!1.0": pep440version{
		epoch:        8,
		release:      []uint32{1, 0},
		wildcarded:   false,
		preType:      nil,
		preNumber:    -1,
		postNumber:   -1,
		devNumber:    -1,
		localVersion: nil,
	},
	"1!2.3.4c5.post6.dev7": pep440version{
		epoch:      1,
		release:    []uint32{2, 3, 4},
		wildcarded: false,
		preType:    &arrsee,
		preNumber:  5,
		postNumber: 6,
		devNumber:  7,
	},
	"4.2rc7": pep440version{
		epoch:        0,
		release:      []uint32{4, 2},
		wildcarded:   false,
		preType:      &arrsee,
		preNumber:    7,
		postNumber:   -1,
		devNumber:    -1,
		localVersion: nil,
	},
	"1.0+foo": pep440version{
		epoch:        0,
		release:      []uint32{1, 0},
		wildcarded:   false,
		preType:      nil,
		preNumber:    -1,
		postNumber:   -1,
		devNumber:    -1,
		localVersion: &foo,
	},
	"1.3.3.7a42": pep440version{
		epoch:        0,
		release:      []uint32{1, 3, 3, 7},
		wildcarded:   false,
		preType:      &ehh,
		preNumber:    42,
		postNumber:   -1,
		devNumber:    -1,
		localVersion: nil,
	},
}

func TestParseVersions(t *testing.T) {
	for v, ex := range parseVersionChecks {
		parsed, err := parseVersion(v)
		if err != nil {
			fmt.Printf("failure to parse %s, error: %s\n", v, err)
			t.FailNow()
		}

		if parsed.epoch != ex.epoch ||
			parsed.wildcarded != ex.wildcarded ||
			parsed.preNumber != ex.preNumber ||
			parsed.postNumber != ex.postNumber ||
			parsed.devNumber != ex.devNumber {
			fmt.Printf("failure on %s (general mismatch)\n", v)
			t.FailNow()
		}

		if parsed.release == nil || len(parsed.release) != len(ex.release) {
			fmt.Printf("failure on %s (release part len mismatch: %d != %d)\n", v, len(parsed.release), len(ex.release))
			t.FailNow()
		}

		for i, n := range ex.release {
			if parsed.release[i] != n {
				fmt.Printf("failure on %s (release part number mismatch)\n", v)
				t.FailNow()
			}
		}

		if ex.preType != nil && (parsed.preType == nil || *parsed.preType != *ex.preType) {
			fmt.Printf("failure on %s (prerelease type mismatch)\n", v)
			t.FailNow()
		}

		if ex.preType == nil && parsed.preType != nil {
			fmt.Printf("failure on %s (extraneous prerelease type)\n", v)
			t.FailNow()
		}

		if ex.localVersion != nil && (parsed.localVersion == nil || *parsed.localVersion != *ex.localVersion) {
			fmt.Printf("failure on %s (local version mismatch)\n", v)
			t.FailNow()
		}

		if ex.localVersion == nil && parsed.localVersion != nil {
			fmt.Printf("failure on %s (extraneous local version)\n", v)
			t.FailNow()
		}
	}
}
