package main

import (
	"io/ioutil"
	"os"
	"path"
	"strings"
)

type sdistformat struct {
	// lol
}

func (sdistformat) getName() string {
	return "sdist"
}

const pkginfo = "PKG-INFO"

func (sdistformat) isPathFormat(ppath string) bool {

	pkginfoPath := path.Join(ppath, pkginfo)

	// Make sure it's not an egg, those are more complicated.
	files, err := ioutil.ReadDir(ppath)
	if err != nil {
		panic(err)
	}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".egg-info") {
			return false
		}
	}

	// Make sure there's no .egg-info file in the dir.
	if _, err := os.Stat(pkginfoPath); err == nil {
		return true
	}

	// Default case.
	return false
}

func (sdistformat) parseMeta(ppath string) *pkgmeta {

	setupdotpyPath := path.Join(ppath, "setup.py")

	pm := &pkgmeta{
		Type:            "bdist",
		Inited:          false,
		SetupScriptPath: nil,
		ImportPath:      nil,
		Deps:            nil,
	}

	// TODO Parse dependencies, idk how to do it for a sdist though.

	if _, err := os.Stat(setupdotpyPath); err == nil {
		pm.SetupScriptPath = &setupdotpyPath
	}

	return pm

}
