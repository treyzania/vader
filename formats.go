package main

type packagefmt interface {
	getName() string
	isPathFormat(string) bool
	parseMeta(string) *pkgmeta
}

func getPackageFormats() []packagefmt {

	var eggfmt = eggformat{}
	var wheelfmt = wheelformat{}
	var sdistfmt = sdistformat{}

	return []packagefmt{
		eggfmt,
		wheelfmt,
		sdistfmt,
	}
}
