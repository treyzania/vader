package main

import (
	"encoding/json"
	"fmt"
	"sort"
)

type verconstraint struct {
	Recognizer func(string) bool
}

func parseVersionOrPanic(v string) *pep440version {
	vv, err := parseVersion(v)
	if err != nil {
		fmt.Printf("failure parsing version %s: %s\n", v, err)
		panic("version parse failure")
	}
	return vv
}

func createConstraint(op, ver string) (*verconstraint, error) {

	var rec func(string) bool
	vv, verr := parseVersion(ver)

	// Yay for core repition.
	if op == "==" && verr == nil {
		rec = func(o string) bool {
			return checkMatchingVersions(*parseVersionOrPanic(o), *vv)
		}
	} else if op == "!=" && verr == nil {
		rec = func(o string) bool {
			return !checkMatchingVersions(*parseVersionOrPanic(o), *vv)
		}
	} else if op == "~=" && verr == nil {
		rec = func(o string) bool {
			return checkCompatibleVersions(*parseVersionOrPanic(o), *vv)
		}
	} else if op == ">=" && verr == nil {
		rec = func(o string) bool {
			return compareVersions(*vv, *parseVersionOrPanic(o)) >= 0
		}
	} else if op == "<=" && verr == nil {
		rec = func(o string) bool {
			return compareVersions(*vv, *parseVersionOrPanic(o)) <= 0
		}
	} else if op == ">" && verr == nil {
		rec = func(o string) bool {
			return compareVersions(*vv, *parseVersionOrPanic(o)) > 0
		}
	} else if op == "<" && verr == nil {
		rec = func(o string) bool {
			return compareVersions(*vv, *parseVersionOrPanic(o)) < 0
		}
	} else if op == "===" {
		rec = func(o string) bool {
			return o == ver
		}
	} else if op == "!==" {
		rec = func(o string) bool {
			return o != ver
		}
	}

	if rec == nil {
		return nil, fmt.Errorf("unrecognized version specifier (%s) or invalid version (%s)", op, ver)
	}

	return &verconstraint{rec}, nil

}

func createConjConstraint(vcs []verconstraint) verconstraint {
	return verconstraint{
		Recognizer: func(o string) bool {
			ok := true
			for _, v := range vcs {
				ok = ok && v.Recognizer(o)
			}
			return ok
		},
	}
}

func createDisjConstraint(vcs []verconstraint) verconstraint {
	return verconstraint{
		Recognizer: func(o string) bool {
			ok := true
			for _, v := range vcs {
				ok = ok || v.Recognizer(o)
			}
			return ok
		},
	}
}

func filterPkgsWithConstraint(pkgs []pippackage, vc verconstraint) []pippackage {
	out := make([]pippackage, 0)
	for _, p := range pkgs {
		if vc.Recognizer(p.Version) {
			out = append(out, p)
		}
	}
	return out
}

func createConstraintFromSelectors(sels []reqselector) verconstraint {
	v := make([]verconstraint, 0)
	for _, s := range sels {
		cc, err := createConstraint(s.Kind, s.Version)
		if err != nil {
			fmt.Printf("invalid selector combination: %s%s\n", s.Kind, s.Version)
			fmt.Println(err.Error())
			panic("selector error")
		}
		v = append(v, *cc)
	}
	if len(v) == 1 {
		return v[0] // just return the only element
	}
	return createConjConstraint(v)
}

func mergeConstraintsMap(vcmaps []map[string]verconstraint) map[string]verconstraint {
	combined := make(map[string][]verconstraint)
	for _, vmap := range vcmaps {
		for pn, vc := range vmap {
			if combined[pn] != nil {
				combined[pn] = append(combined[pn], vc)
			} else {
				combined[pn] = make([]verconstraint, 1)
				combined[pn][0] = vc
			}
		}
	}
	conj := make(map[string]verconstraint)
	for k, v := range combined {
		if len(v) == 1 { // If there's only one then just keep it as-is.
			conj[k] = v[0]
		} else if len(v) > 1 {
			conj[k] = createConjConstraint(v) // AND
		} else {
			// do nothing, we don't need it
		}
	}
	return conj
}

func makeConstraintMapFromKnownBad(kb map[string][]string) map[string]verconstraint {
	out := make(map[string]verconstraint)
	for k, v := range kb {
		preds := make([]verconstraint, 0)
		for _, vs := range v {
			vc, err := createConstraint("!==", vs)
			if err != nil {
				preds = append(preds, *vc)
			} else {
				fmt.Printf("warning: Can't create constraint for version %s (from package %s)", vs, k)
			}
		}
		out[k] = createConjConstraint(preds)
	}
	return out
}

func pickVersionFromDef(pyver string, reqdef requirementdef) (*pippackage, error) {

	if reqdef.URL != nil {
		return nil, fmt.Errorf("URLs not supported yet: %s @ %s", reqdef.Name, *reqdef.URL)
	}

	for _, k := range reqdef.Selectors {
		println(k.Kind + " " + k.Version)
	}

	j, _ := json.Marshal(reqdef)
	fmt.Printf("%s\n", j)

	vc := createConstraintFromSelectors(reqdef.Selectors)

	pkgsofname := make([]pippackage, 0)

	// Create package ID objects for each of the versions of the packages.
	vers := cachingGetPkgVersions(reqdef.Name)
	for _, v := range vers {
		if vc.Recognizer(v) {
			pkgsofname = append(pkgsofname, pippackage{
				Pipver:  pyver,
				Name:    reqdef.Name,
				Version: v,
			})
		}
	}

	if len(pkgsofname) == 0 {
		return nil, fmt.Errorf("no packages found for package %s and selectors", reqdef.Name)
	}

	// Now sort the packages by version.
	sort.Sort(sortByVersion(pkgsofname))

	// Now pick the latest.
	latest := pkgsofname[len(pkgsofname)-1]
	return &latest, nil

}

func resoloveDependencies(pyver string, basepackages []requirementdef) []pippackage {

	decided := make(map[string]pippackage)
	declist := make([]pippackage, 0)

	queue := make([]requirementdef, 0)

	// Pull in all the base packages, those just select everything out of the box.
	for _, bp := range basepackages {
		picked, err := pickVersionFromDef(pyver, bp)
		if err != nil {
			fmt.Printf("problem deciding root package version: %s", err)
			panic(err)
		}

		// Record it, then build it.
		decided[bp.Name] = *picked
		declist = append(declist, *picked)
		downloadPackage(*picked)

		// Now figure out which packages we need to get in the next iteration.
		pspec := picked.getMeta().Deps
		for _, br := range pspec.BaseReqs {
			ok := true
			for _, de := range decided {
				if de.Name == br.Name {
					ok = false
				}
			}
			if ok {
				queue = append(queue, br)
			}
		}
	}

	// Now we work on all the subpackages.
	for len(queue) != 0 {

		// Put everything into this queue, we replace the old one with this one afterwards.
		nextqueue := make([]requirementdef, 0)

		for _, qe := range queue {

			// See if we already have it somewhere.
			ok := true
			if _, found := decided[qe.Name]; found {
				ok = false
			}
			for _, nqe := range nextqueue {
				if nqe.Name == qe.Name {
					ok = false
				}
			}

			if ok {

				// Now we decide which package we want, right now we pick the latest applicable.
				picked, err := pickVersionFromDef(pyver, qe)
				if err != nil {
					fmt.Printf("problem deciding subpackage package version: %s\n", err)
					panic(err)
				}

				// Record it, then build it.
				fmt.Printf("Preparing package %s...\n", picked.getDescriptorStr())
				decided[qe.Name] = *picked
				declist = append(declist, *picked)
				downloadPackage(*picked)

				// Pull in elements for the next iteration, if available.
				pspec := picked.getMeta().Deps
				if pspec != nil {
					for _, pde := range pspec.BaseReqs {
						ok := true
						for _, nqe := range nextqueue {
							if nqe.Name == pde.Name {
								ok = false
							}
						}
						if ok {
							nextqueue = append(nextqueue, pde)
						}
					}
				} else {
					fmt.Printf("no deps found for %s\n", picked.getDescriptorStr())
				}

			}

			queue = nextqueue

		}

	}

	// Now build packages in reverse order.
	i := len(declist) - 1
	for i >= 0 {
		dep := declist[i]
		dm := dep.getMeta()
		if dm.Inited || dm.SetupScriptPath == nil {
			continue
		}

		fmt.Printf("Building package %s...\n", dep.getDescriptorStr())
		err := buildPackage(dep, []pippackage{})
		if err != nil {
			fmt.Printf("failed to build package: %s\n", err)
			panic("pkg build error")
		}

		i--
	}

	// Now just turn it into a list.
	out := make([]pippackage, 0)
	for _, p := range decided {
		out = append(out, p)
	}
	return out

}

/*
 * mmmmm  mmmmmm mmmmmm mmmmmm mmmmmm mmmmmm mmmmmm mmmmmm mmmmmm mmmmmm
 * #   "# #      #      #      #      #      #      #      #      #
 * #mmmm" #mmmmm #mmmmm #mmmmm #mmmmm #mmmmm #mmmmm #mmmmm #mmmmm #mmmmm
 * #   "m #      #      #      #      #      #      #      #      #
 * #    " #mmmmm #mmmmm #mmmmm #mmmmm #mmmmm #mmmmm #mmmmm #mmmmm #mmmmm
 */
type sortByVersion []pippackage

func (s sortByVersion) Len() int {
	return len(s)
}

func (s sortByVersion) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s sortByVersion) Less(i, j int) bool {

	iv, err := parseVersion(s[i].Version)
	if err != nil {
		fmt.Printf("failure to parse version during sort: %s\n", s[i].Version)
		panic("version parse error")
	}

	jv, err := parseVersion(s[j].Version)
	if err != nil {
		fmt.Printf("failure to parse version during sort: %s\n", s[j].Version)
		panic("version parse error")
	}

	c := compareVersions(*iv, *jv)

	return c > 0 // I think this is right lol
}
