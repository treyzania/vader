package main

import (
	"encoding/json"
	"fmt"
	"testing"
)

var (
	parseReqsChecks = map[string]bool{
		"foo==1.2.3":         true,
		"foo == 1.2.3":       true,
		"bar>=2.3.6a4":       true,
		"baz >= 5.0, != 5.1": true,
	}
)

func TestParseRequirement(t *testing.T) {

	for k := range parseReqsChecks {
		p := parseRequirement(k)
		d, err := json.MarshalIndent(p, "", "  ")
		if err != nil {
			fmt.Printf("%s\n", err.Error())
			t.Fail()
			return
		}
		fmt.Printf("%s:\n%s\n\n", k, d)
	}

}
