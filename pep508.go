package main

import (
	"bufio"
	"os"
	"regexp"
	"strings"
)

type requirementsspec struct {
	BaseReqs    []requirementdef
	FeatureReqs map[string][]requirementdef
}

type requirementdef struct {
	Name      string
	Selectors []reqselector
	URL       *string
}

type reqselector struct {
	Kind    string
	Version string
}

const pkgNameRegex = `^[\w\-\.]+`
const pkgVerRegex = `^[0-9]+(\.[0-9]+)+$`
const featureRegex = `\[\w+\]`
const vselRegex = `===|([<>=!~]=)|>|<`

func parseRequirement(line string) requirementdef {

	// example: requests [security,tests] >= 2.8.1, == 2.8.* ; python_version < "2.7"

	// Clean ends, just in case.
	linestrip := strings.TrimSpace(line)

	// Parse the name of the package.
	nr := regexp.MustCompile(pkgNameRegex)
	name := strings.ToLower(nr.FindString(linestrip)) // also lowercase it

	// Figure out which versions we're using, including removing the channel strings `[like,this]`.
	postNameStr := linestrip[len(name):]
	postNameStrs := strings.Split(postNameStr, ";")
	fr := regexp.MustCompile(featureRegex)
	verSpecStrs := strings.Split(strings.TrimSpace(fr.ReplaceAllString(postNameStrs[0], "")), ",")

	// This is where we actually find them out.
	sels := make([]reqselector, 0)
	if len(postNameStrs[0]) > 0 {
		sr := regexp.MustCompile(vselRegex)
		for _, s := range verSpecStrs {
			trimmed := strings.TrimSpace(s)
			kind := sr.FindString(trimmed)
			ver := strings.TrimSpace(trimmed[len(kind):])
			sels = append(sels, reqselector{
				Kind:    kind,
				Version: ver,
			})
		}
	}

	// TODO Parse features.
	// TODO Parse URLs.
	// TODO Parse extra data, like `python_version < "2.7"`

	return requirementdef{
		Name:      name,
		Selectors: sels,
		URL:       nil,
	}
}

func parseRequirementsFile(path string) requirementsspec {

	file, _ := os.Open(path)
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	reqs := make([]requirementdef, 0)

	var currentfeature *string
	features := make(map[string][]requirementdef)

	for scanner.Scan() {
		line := scanner.Text()
		if len(line) <= 2 { // covers empy lines
			continue
		}

		// Feature section dectection.
		if strings.HasPrefix(line, "[") && strings.HasSuffix(line, "]") {
			cf := line[1:1]
			currentfeature = &cf
			continue
		}

		// Now we just have to figure out which list we're putting it into.
		parsed := parseRequirement(line)
		if currentfeature == nil {
			reqs = append(reqs, parsed)
		} else {
			features[*currentfeature] = append(features[*currentfeature], parsed)
		}
	}

	return requirementsspec{
		BaseReqs:    reqs,
		FeatureReqs: features,
	}

}
