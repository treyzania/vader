package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"syscall"

	"gopkg.in/yaml.v2"
)

type bindef struct {
	Path  string
	Pyver string
}

func findPythonBins() []bindef {

	var path = os.Getenv("PATH")
	var pythons = make([]bindef, 0)
	for _, s := range strings.Split(path, ":") {
		var programs, _ = ioutil.ReadDir(s)
		for _, p := range programs {
			var pn = p.Name()
			if strings.HasPrefix(pn, "python") && !strings.Contains(pn, "-") {
				var e = bindef{}
				e.Path = s + "/" + pn
				e.Pyver = strings.TrimPrefix(pn, "python")
				pythons = append(pythons, e)
			}
		}
	}

	return pythons

}

type vaderfiledef struct {
	Main  string   `yaml:"main"`
	Pyver string   `yaml:"pyver"`
	Deps  []string `yaml:"deps"`
}

func parseVaderfile(path string) vaderfiledef {

	// Mostly stolen from https://stackoverflow.com/questions/28682439/

	filename, _ := filepath.Abs(path)
	yamlfile, err := ioutil.ReadFile(filename)

	// MEME
	if err != nil {
		panic(err)
	}

	var out vaderfiledef
	err = yaml.Unmarshal(yamlfile, &out)

	// MEME
	if err != nil {
		panic(err)
	}

	return out

}

func execVaderfile(vf vaderfiledef) {

	// Parse the requirement definitions
	bpkgs := make([]requirementdef, len(vf.Deps))
	for i, ddef := range vf.Deps {
		bpkgs[i] = parseRequirement(ddef)
	}

	// Now figure out the PYTHONPATH variable.
	resolved := resoloveDependencies(vf.Pyver, bpkgs)

	/*
		// TODO Do we actually need this?
		// Make a fake PYTHONHOME so we don't get conflicts or anything.
		pythonhome, err := ioutil.TempDir("", "vader.pythonhome.")
		if err != nil {
			panic(err)
		}
	*/

	invokePythonWithEnv(vf.Pyver, []string{vf.Main}, resolved, nil)

}

func setupDumpstack() {
	go func() {
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGQUIT)
		buf := make([]byte, 1<<20)
		for {
			<-sigs
			stacklen := runtime.Stack(buf, true)
			fmt.Printf("=== received SIGQUIT ===\n*** goroutine dump...\n%s\n*** end\n", buf[:stacklen])
		}
	}()
}

func main() {

	setupDumpstack()

	args := os.Args[1:]
	if len(args) < 1 {
		println("not enough arguments")
		return
	}

	verb := args[0]

	if verb == "run" {

		var vf = parseVaderfile("./Vaderfile")
		execVaderfile(vf)

	} else if verb == "pull" {

		if len(args) != 4 {
			panic("not enough args")
		}

		pyver := args[1]
		pkgname := args[2]
		pkgver := args[3]

		pkg := pippackage{
			Pipver:  pyver,
			Name:    pkgname,
			Version: pkgver,
		}

		downloadPackage(pkg)
		buildPackage(pkg, []pippackage{}) // TODO Figure out deps properly.

	} else if verb == "diag-lspy" {

		var pys = findPythonBins()
		for _, bd := range pys {
			println("python" + bd.Pyver + " " + bd.Path)
		}

	} else if verb == "diag-parsereq" {

		println(args[1] + "\n")
		req := parseRequirement(args[1])
		j, _ := json.MarshalIndent(req, "", "\t")
		println(string(j))

	} else if verb == "diag-importpath" {

		pyver := args[1]
		pkgname := args[2]
		pkgver := args[3]

		pkg := pippackage{
			Pipver:  pyver,
			Name:    pkgname,
			Version: pkgver,
		}

		println(*pkg.getPkgImportPath("lib"))

	} else if verb == "diag-getversions" {

		name := args[1]

		latest := getPkgLatestVersion(name)
		fmt.Printf("Latest: %s==%s\nAll:\n", name, latest)

		vers := getPkgVersions(name)
		for _, v := range vers {
			fmt.Printf("  - %s\n", v)
		}

	} else if verb == "diag-cmpvers" {

		v1s := args[1]
		v2s := args[2]

		v1, err := parseVersion(v1s)
		if err != nil {
			panic(err)
		}
		v2, err := parseVersion(v2s)
		if err != nil {
			panic(err)
		}

		cmp := compareVersions(*v1, *v2)
		fmt.Printf("compareVersions(%s,%s) == %s\n", v1s, v2s, strconv.Itoa(cmp))

	} else {
		println("bad")
	}

}
