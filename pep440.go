package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

const pep440VersionRegex = `^([1-9]\d*!)?(0|[1-9]\d*)(\.(0|[1-9]\d*))*((a|b|r?c)(0|[1-9]\d*))?(\.post(0|[1-9]\d*))?(\.dev(0|[1-9]\d*))?$`
const pep440LocalRegex = `^[a-zA-Z0-9\.]+$`

type pep440version struct {
	epoch        uint32
	release      []uint32
	wildcarded   bool // if there's a ".*" at the end of the number versioned release
	preType      *string
	preNumber    int32
	postNumber   int32
	devNumber    int32
	gitCommit    *string
	localVersion *string // the ""+foobar" at the end of versions sometimes
}

func (v *pep440version) IsFinal() bool {
	return !v.wildcarded && v.preType == nil && v.preNumber == -1 && v.postNumber == -1 && v.devNumber == -1
}

func (v *pep440version) GetReleaseIndex(i uint) uint32 {
	if int(i) >= len(v.release) {
		return 0
	}
	return v.release[i]
}

func (v *pep440version) ToString() string {
	comp := ""

	if v.epoch != 0 {
		comp = strconv.Itoa(int(v.epoch)) + "!"
	}

	for i, c := range v.release {
		if i != 0 {
			comp += "."
		}
		comp += strconv.Itoa(int(c))
	}

	if v.wildcarded {
		comp += ".*"
	}

	if v.preType != nil {
		comp += *v.preType + strconv.Itoa(int(v.preNumber))
	}

	if v.postNumber != -1 {
		comp += ".post" + strconv.Itoa(int(v.postNumber))
	}

	if v.devNumber != -1 {
		comp += ".dev" + strconv.Itoa(int(v.devNumber))
	}

	if v.localVersion != nil {
		comp += "+" + *v.localVersion
	}

	return comp

}

var (
	preReleaseSignifier = []string{
		"a",
		"b",
		"rc",
		"c", // This has to be this way otherwise things get confused.
	}
	substitutions = map[string]string{
		"alpha": "a",
		"beta":  "b",
	}
	postReleasePrefixes = []string{
		"post",
		"rev",
		"r",
	}
	devReleasePrefix = "dev"
)

var (
	arrsee = "rc" // WHY THE FUCK DO I HAVE TO DO THIS
)

func parseVersion(s string) (*pep440version, error) {

	// This is what we're returning, modifying in the rest of the function as we find things.
	v := &pep440version{
		epoch:        0, // left undefined, assume 0
		release:      make([]uint32, 0),
		wildcarded:   false,
		preType:      nil,
		preNumber:    -1,
		postNumber:   -1,
		devNumber:    -1,
		localVersion: nil,
	}

	remaining := strings.TrimPrefix(s, "v")

	// Strip out local version info, which is entirely separate.
	if strings.Contains(remaining, "+") {
		parts := strings.SplitN(remaining, "+", 2)
		v.localVersion = &parts[1]
		remaining = parts[0]
	}

	// Lowercase it, of course.
	remaining = strings.ToLower(remaining)

	// Do some replacements becase we're lazy and don't want to support too much shit.
	for o, r := range substitutions {
		remaining = strings.Replace(remaining, o, r, -1)
	}

	// Parse epochs, 0 if left undefined, as above.
	if strings.Contains(remaining, "!") {
		parts := strings.SplitN(remaining, "!", 2)
		en, err := strconv.Atoi(parts[0])
		if err != nil || en < 0 {
			return nil, fmt.Errorf("invalid epoch %s in %s", parts[0], s)
		}
		v.epoch = uint32(en)
		remaining = parts[1]
	}

	// Now for the annoying part, handling the rest of this stuff.
	dotted := strings.Split(remaining, ".")
	for ri, p := range dotted {

		// Also support wildcard versions since it simplifies the parsing logic.
		if p == "*" {
			if v.preNumber != -1 || v.postNumber != -1 || v.devNumber != -1 || v.gitCommit != nil {
				return nil, fmt.Errorf("invalid place for a wildcard in %s", s)
			}
			v.wildcarded = true
			break
		}

		// This isn't part of the standard, but we gotta do it for some stupid shit.
		if strings.HasPrefix(p, "g") {
			commit := p[1:]
			v.gitCommit = &commit // TODO verify [0-9a-f]
			continue
		}

		// First check for post releases
		for _, post := range postReleasePrefixes {
			if strings.HasPrefix(p, post) {
				num := p[len(post):]
				pvn, err := strconv.Atoi(num)
				if err != nil || pvn < 0 {
					return nil, fmt.Errorf("invalid post release %s in %s", num, s)
				}
				v.postNumber = int32(pvn)
				continue
			}
		}

		// And dev releases
		if strings.HasPrefix(p, devReleasePrefix) {
			num := p[len(devReleasePrefix):]
			dvn, err := strconv.Atoi(num)
			if err != nil || dvn < 0 {
				return nil, fmt.Errorf("invalid dev release %s in %s", num, s)
			}
			v.devNumber = int32(dvn)
			continue
		}

		// Make sure we aren't going to start parsing numbers again if we get to things after numbers.
		if v.preNumber != -1 || v.postNumber != -1 || v.preType != nil {
			return nil, fmt.Errorf("invalid place for numbers in %s", s)
		}

		// Just try to parse it straight away.
		releasenum, err := strconv.Atoi(p)

		// If there's an error it might be because this chunk is a preprelease thing.
		if err != nil {

			// Check all of the prerelease type kind things.
			precomp := false
			for _, pre := range preReleaseSignifier {
				if strings.Contains(p, pre) {

					v.preType = &pre

					// Special case in the standard, "c" should be interpreted as "rc".
					if pre == "c" {
						v.preType = &arrsee
					}

					// Parse the *actual* release part number, if there is one.
					parts := strings.SplitN(p, pre, 2)
					if len(parts[0]) > 0 { // it has length 0 if there's nothing there.
						var rn int
						rn, err = strconv.Atoi(parts[0])
						if err != nil || rn < 0 {
							return nil, fmt.Errorf("invalid release version component index %d in %s", ri, s)
						}
						v.release = append(v.release, uint32(rn))
					}

					// Parse the prerelease number.
					if len(parts[1]) > 0 {
						var pn int
						pn, err = strconv.Atoi(parts[1])
						if err != nil || pn < 0 {
							return nil, fmt.Errorf("invalid prerelease version %d in %s", pn, s)
						}
						v.preNumber = int32(pn)
					} else {
						// this should be very rare, so far I've only seen this
						// case in `sqlalchemy`
						v.preNumber = 1
					}

					// Then set us up to exit the loop here and go to the next iter of the outer loop.
					precomp = true
					break
				}
			}

			// This is for doing stupid stuff like 1.2.3rev5 which stupid things like configparser do.
			if !precomp {
				for _, post := range postReleasePrefixes {
					if strings.Contains(p, post) {

						parts := strings.SplitN(p, post, 2)

						if len(parts[0]) > 0 {
							var rn int
							rn, err = strconv.Atoi(parts[0])
							if err != nil || rn < 0 {
								return nil, fmt.Errorf("invalid release version component index %d in %s", ri, s)
							}
							v.release = append(v.release, uint32(rn))
						}

						if len(parts[1]) > 0 {
							var pn int
							pn, err = strconv.Atoi(parts[1])
							if err != nil || pn < 0 {
								return nil, fmt.Errorf("invalid postrelease %s in %s", parts[1], s)
							}
							v.postNumber = int32(pn)
						}

						precomp = true
						break
					}
				}
			}

			// We should exit and continue, probably about to parse a .postX or a .devX thing.
			if precomp {
				continue
			}
			return nil, fmt.Errorf("invalid \"thing\" %s in %s", p, s)
		}

		if releasenum < 0 {
			return nil, fmt.Errorf("invalid release version component %d in %s", releasenum, s)
		}

		// Append the release number to the array.
		v.release = append(v.release, uint32(releasenum))

	}

	return v, nil

}

var (
	// Used when comparing prerelease types.
	preTypeCmp = map[string]int{
		"aa":   0,
		"ab":   1,
		"arc":  1,
		"ba":   -1,
		"bb":   0,
		"brc":  1,
		"rca":  -1,
		"rcb":  -1,
		"rcrc": 0,
	}
)

// This is like your standard version comparison function.  Note that it's not
// 100% accurate to the standard, but should work for our purposes.  See below:
// * https://www.python.org/dev/peps/pep-0440/#summary-of-permitted-suffixes-and-relative-ordering
// * https://www.python.org/dev/peps/pep-0440/#normalization
// Also you'll get confusing results with wildcard versions, probably.
func compareVersions(a, b pep440version) int {

	// If they have different epochs then they're obviously different.
	if a.epoch < b.epoch {
		return 1
	} else if a.epoch > b.epoch {
		return -1
	}

	if a.release == nil ||
		b.release == nil ||
		(len(a.release) == 0 && !a.wildcarded) ||
		(len(b.release) == 0 && !b.wildcarded) {
		panic("versions have nil or empty release versions") // wtf?
	}

	if (len(a.release) == 0 && a.wildcarded) || (len(b.release) == 0 && b.wildcarded) {
		return 0
	}

	// Check for primary revision numbers.
	i := 0
	arlen := len(a.release)
	brlen := len(b.release)
	for i < arlen || i < brlen {

		ai := a.GetReleaseIndex(uint(i))
		bi := b.GetReleaseIndex(uint(i))
		//fmt.Printf("\t%d: %d %d\n", i, ai, bi)

		if ai < bi {
			return 1
		} else if ai > bi {
			return -1
		}

		i++
	}

	// Compare prerelease types, if applicable.
	// The latter 2 thing here I can't explain my reasoning but I'm 95% sure it's right.
	if a.preType != nil && b.preType != nil {

		// Look up the pretype
		pt := preTypeCmp[*a.preType+*b.preType]

		// We've found a discrepancy!
		if pt != 0 {
			return pt
		}

		// At this point we're only worrying about prerelease numbers.
		if a.preNumber < b.preNumber {
			return 1
		} else if a.preNumber > b.preNumber {
			return -1
		}

	} else if a.preType != nil && b.preType == nil {
		return 1
	} else if a.preType == nil && b.preType != nil {
		return -1
	}

	// Compare post-release numbers.
	if a.postNumber != -1 && b.postNumber != -1 {
		if a.postNumber < b.postNumber {
			return 1
		} else if a.postNumber > b.postNumber {
			return -1
		}
	} else if a.postNumber != -1 && b.postNumber == -1 {
		return -1
	} else if a.postNumber == -1 && b.postNumber != -1 {
		return 1
	}

	// Compare dev release numbers.
	if a.devNumber != -1 && b.devNumber != -1 {
		if a.devNumber < b.devNumber {
			return 1
		} else if a.preNumber > b.preNumber {
			return -1
		}
	} else if a.devNumber != -1 && b.devNumber == -1 {
		return -1
	} else if a.devNumber == -1 && b.devNumber != -1 {
		return 1
	}

	// At this point, I've tried everything.  Just say it's *probably* equal.
	// (local version numbers should just be ignored)
	return 0

}

func isPep440(v string) bool {
	parts := strings.Split(v, "+")

	// Public version identifier.
	r := regexp.MustCompile(pep440VersionRegex)
	matches := r.MatchString(parts[0])

	// TODO Local verson label.

	return matches
}

func checkCompatibleVersions(ver, filter pep440version) bool {

	if filter.localVersion != nil {
		fmt.Printf("filter has a local version in %s", filter.ToString())
		panic("invalid filter")
	}

	if ver.epoch != filter.epoch {
		return false
	}

	frl := len(filter.release)
	if frl <= 1 {
		fmt.Printf("bad version in filter %s", filter.ToString())
		panic("invalid filter")
	}

	// Prefix matching?
	if len(filter.release) < len(ver.release) && filter.wildcarded {

		for i := range filter.release {
			if ver.release[i] != filter.release[i] {
				return false
			}
		}

		return true

	}

	// TODO Other matching.

	return false
}

func checkMatchingVersions(ver, filter pep440version) bool {

	if filter.localVersion != nil {
		fmt.Printf("filter has a local version in %s", filter.ToString())
		panic("invalid filter")
	}

	if ver.epoch != filter.epoch {
		return false
	}

	// Prefix matching?
	if filter.wildcarded {
		if len(filter.release) <= len(ver.release) {
			for i := range filter.release {
				if ver.release[i] != filter.release[i] {
					return false
				}
			}
			return true
		}
		return false
	}

	// At this point it's just pure equality.
	i := 0
	for i < len(ver.release) || i < len(filter.release) {

		// This kind of comparison does 0-padding in undefined version indexes.
		if ver.GetReleaseIndex(uint(i)) != filter.GetReleaseIndex(uint(i)) {
			return false
		}

		i++
	}

	if ver.preType != filter.preType {
		return false
	}

	if ver.preNumber != filter.preNumber {
		return false
	}

	if ver.postNumber != filter.postNumber {
		return false
	}

	if ver.devNumber != filter.devNumber {
		return false
	}

	return true
}
