package main

import (
	"bufio"
	"io/ioutil"
	"os"
	"path"
	"strings"
)

func parseWheelMetaForHeaders(filepath string, headers []string) ([]string, []string, error) {

	keys := make([]string, 0)
	values := make([]string, 0)

	file, err := os.Open(filepath)
	if err != nil {
		return nil, nil, err
	}

	scan := bufio.NewScanner(file)
	scan.Split(bufio.ScanLines)

	for scan.Scan() {

		line := scan.Text()

		for _, header := range headers {

			match := header + ": "
			if strings.HasPrefix(line, match) {
				keys = append(keys, header)
				values = append(values, line[len(match):])
			}

			// If we've reached an empty line we're after the headers now.
			if line == "" {
				break
			}

		}

	}

	return keys, values, nil

}

func parseWheelDeps(filepath string) requirementsspec {

	// FIXME THIS IS NOT AT ALL HOW THESE SHOULD ACTUALLY BE PARSED.

	headers, values, err := parseWheelMetaForHeaders(filepath, []string{"Requires-Dist"})
	if err != nil {
		panic(err)
	}

	base := make([]requirementdef, 0)
	extras := make(map[string][]requirementdef)

	var feature *string

	for i, h := range headers {

		rawstr := values[i]
		reqstr := strings.Replace(strings.Replace(rawstr, "(", "", -1), ")", "", -1)
		req := parseRequirement(reqstr)

		if h == "Requires-Dist" {
			if feature == nil {
				base = append(base, req)
			} else {
				extras[*feature] = append(extras[*feature], req)
			}
		} else if h == "Provides-Extra" {
			feature = &values[i]
		} else {
			panic("wtf")
		}

	}

	return requirementsspec{
		BaseReqs:    base,
		FeatureReqs: extras,
	}

}

type wheelformat struct {
	// idk
}

func (wheelformat) getName() string {
	return "wheel"
}

func (wheelformat) isPathFormat(ppath string) bool {

	files, err := ioutil.ReadDir(ppath)
	if err != nil {
		return false
	}

	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".dist-info") {
			return true
		}
	}

	return false

}

func (wheelformat) parseMeta(ppath string) *pkgmeta {

	files, err := ioutil.ReadDir(ppath)
	if err != nil {
		panic(err)
	}

	var metapath string

	for _, f := range files {
		n := f.Name()

		if strings.HasSuffix(n, ".dist-info") {
			metapath = n
		}
	}

	deps := parseWheelDeps(path.Join(ppath, metapath, "METADATA"))

	return &pkgmeta{
		Type:            "wheel",
		Inited:          true,
		SetupScriptPath: nil,
		ImportPath:      &ppath,
		Deps:            &deps,
	}

}
