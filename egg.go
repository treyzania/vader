package main

import (
	"fmt"
	"io/ioutil"
	"path"
	"strings"
)

type eggformat struct {
	// idk
}

func (eggformat) getName() string {
	return "egg"
}

func (eggformat) isPathFormat(ppath string) bool {

	files, err := ioutil.ReadDir(ppath)
	if err != nil {
		panic(err)
	}

	for _, f := range files {
		if f.Name() == "EGG-INFO" {
			return true
		} else if strings.HasSuffix(f.Name(), ".egg-info") {
			return true
		}
	}

	return false

}

func (eggformat) parseMeta(ppath string) *pkgmeta {

	files, err := ioutil.ReadDir(ppath)

	if err != nil {
		panic(err)
	}

	var metapath string
	var setuppath string

	for _, f := range files {

		n := f.Name()

		if n == "EGG-INFO" {

			// then we're in the plain `.egg` format.
			metapath = path.Join(ppath, "EGG-INFO")

		} else if strings.HasSuffix(n, ".egg-info") {

			// we're probably in the `.egg-info` format, figure now make sure
			metapath = path.Join(ppath, n)

		} else if f.Name() == "setup.py" {

			// Regardless of the format, this is the setup.py file that should be run.
			setuppath = path.Join(ppath, n)

		} else {
			// skip
		}

	}

	metafiles, err := ioutil.ReadDir(metapath)

	if err != nil {
		fmt.Printf("not an egg: %s\n", err)
		return nil // not an egg
	}

	var reqspec requirementsspec

	for _, f := range metafiles {

		n := f.Name()

		if n == "requires.txt" {
			reqspec = parseRequirementsFile(path.Join(metapath, n))
		}

	}

	return &pkgmeta{
		Type:            "egg",
		Inited:          false,
		SetupScriptPath: &setuppath,
		ImportPath:      nil, // this should be nil
		Deps:            &reqspec,
	}

}
