#!/usr/bin/env python3

import os
import time

from flask import Flask

app = Flask(__name__)

@app.route('/')
def route():
    return 'Hello, World!\n'

# Usually don't actually run it because it has to exit.
if os.getenv('ACTUALLY_RUN') == '1':
    app.run()
else:
    print('OK')
    time.sleep(1)
