package main

import (
	"fmt"
	"os"
	"os/exec"
)

func getPythonPathEnvvarSpec(pkgs []pippackage) string {
	pypath := ""

	if len(pkgs) >= 1 {

		for i, p := range pkgs {

			ipath := p.getMeta().ImportPath
			if ipath == nil {
				fmt.Printf("Tried to import package without import path, is it fully initialized? (%s)\n", p.getDescriptorStr())
				panic("tried to import unimportable package")
			}

			if i == 0 {
				pypath = *ipath
			} else {
				pypath = fmt.Sprintf("%s:%s", pypath, *ipath) // probably different on windows, but fuck it we'll deal with that later
			}

		}

	}

	return pypath
}

func invokePythonWithEnv(pyver string, suffix []string, deps []pippackage, dir *string) {

	bin := "python" + pyver

	pythonpath := getPythonPathEnvvarSpec(deps)

	args := make([]string, 0)
	args = append(args, "-s", "-S")
	args = append(args, suffix...)

	// Actually execute.
	var prog = exec.Command(bin, args...)
	prog.Env = append(os.Environ(), "PYTHONPATH="+pythonpath)
	if dir != nil {
		prog.Dir = *dir
	}
	prog.Stdin = os.Stdin
	prog.Stdout = os.Stdout
	prog.Stderr = os.Stderr
	prog.Run()

}
